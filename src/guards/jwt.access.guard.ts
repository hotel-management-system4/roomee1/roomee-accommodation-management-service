import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { ForbiddenException, UnauthorizedException } from '@roomee1/common';
import { Request } from 'express';
import { TokenOptions, UserData } from 'types';

@Injectable()
export class JwtAccessGuard implements CanActivate {
  private readonly accessTokenOptions: TokenOptions;
  constructor(
    configService: ConfigService,
    private jwtService: JwtService,
  ) {
    this.accessTokenOptions = configService.get<TokenOptions>('accessToken');
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest<Request>();
    const authorization = request.headers['authorization'];
    if (!authorization) {
      throw new UnauthorizedException('Token not appear in header');
    }

    const token = authorization.split(' ')[1];
    if (!token) {
      throw new UnauthorizedException('Not supported token strategy');
    }

    const user = await this.jwtService.verifyAsync<UserData>(token, {
      publicKey: this.accessTokenOptions.publicKey,
    });

    if (!user) {
      return false;
    }

    if (user.banned) {
      throw new ForbiddenException('user is banned');
    }

    // if (!user.emailVerified) {
    //   throw new ForbiddenException('user must verified your email');
    // }

    delete user['iat'];
    delete user['exp'];
    delete user['iss'];
    delete user['sub'];
    request.user = user;

    return true;
  }
}
