import {
  CanActivate,
  ExecutionContext,
  Inject,
  Injectable,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import {
  ForbiddenException,
  IRedisCacheService,
  UnauthorizedException,
} from '@roomee1/common';
import { Request } from 'express';
import { TokenOptions, UserData } from 'types';

@Injectable()
export class JwtRefreshGuard implements CanActivate {
  private readonly refreshTokenOptions: TokenOptions;
  constructor(
    configService: ConfigService,
    private jwtService: JwtService,
    @Inject(IRedisCacheService)
    private redisCacheService: IRedisCacheService,
  ) {
    this.refreshTokenOptions = configService.get<TokenOptions>('refreshToken');
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest<Request>();
    const authorization = request.headers['authorization'];
    const device = request.headers['device-id'];
    if (!authorization) {
      throw new UnauthorizedException('Token not appear in header');
    }

    const token = authorization.split(' ')[1];
    if (!token) {
      throw new UnauthorizedException('Not supported token strategy');
    }

    const user = await this.jwtService.verifyAsync<UserData>(token, {
      publicKey: this.refreshTokenOptions.publicKey,
    });

    if (!user) {
      return false;
    }

    if (user.banned) {
      throw new ForbiddenException('user is banned');
    }

    const oldRefreshToken = await this.redisCacheService.get(
      `${user.id}:device:${device}`,
    );

    if (oldRefreshToken !== token) {
      throw new ForbiddenException('Invalid token');
    }

    delete user['iat'];
    delete user['exp'];
    delete user['iss'];
    delete user['sub'];
    request.user = user;

    return true;
  }
}
