import { ApiProperty } from '@nestjs/swagger';

export class UserResponse {
  @ApiProperty({
    type: String,
    default: 'MyUsername',
  })
  username: string;

  @ApiProperty({
    type: String,
    default: 'Andrew',
  })
  firstName: string;

  @ApiProperty({
    type: String,
    default: 'NG',
  })
  lastName: string;

  @ApiProperty({
    type: String,
    default: '223232a',
  })
  id: string;

  @ApiProperty({
    type: Boolean,
    default: false,
  })
  emailVerified: boolean;

  @ApiProperty({
    type: Boolean,
    default: false,
  })
  phoneVerified: boolean;

  @ApiProperty({
    type: String,
    default: '0969486904',
  })
  phoneNumber: string;

  @ApiProperty({
    type: String,
    default: 'TENANT',
  })
  role: string;

  @ApiProperty({
    type: String,
    default: 'http://google.com/123.png',
  })
  imageUrl: string;

  @ApiProperty({
    type: String,
    default: '12/08/2002',
  })
  birthday: string;
}

export class DeviceResponse {
  @ApiProperty({
    type: String,
    default: 'uuid',
  })
  key: string;
  @ApiProperty({
    type: String,
    default: 'device name',
  })
  deviceName: string;
}

export class TrackingResponse {
  @ApiProperty({
    type: String,
    default: 'uuid',
  })
  trackingId: string;
}

export class UserDeviceResponse {
  @ApiProperty({
    type: UserResponse,
  })
  user: UserResponse;
  @ApiProperty({
    type: Array<DeviceResponse>,
  })
  devices: DeviceResponse[];
}
