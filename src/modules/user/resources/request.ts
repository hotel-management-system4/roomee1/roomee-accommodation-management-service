import { ApiProperty } from '@nestjs/swagger';
import { RoleList } from '@prisma/client';
import { defaultValue, parseInt } from '@roomee1/common';
import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsEmail,
  IsNumber,
  IsString,
  IsStrongPassword,
  ValidateNested,
} from 'class-validator';

export class VerifyDTO {
  @ApiProperty({
    default: 1234,
    type: Number,
  })
  @IsNumber()
  verifyCode: number;
}

export class UpdateUserProfileDTO {
  @ApiProperty({
    default: 'firstname',
    type: String,
  })
  @IsString()
  firstName: string;

  @ApiProperty({
    default: 'lastname',
    type: String,
  })
  @IsString()
  lastName: string;

  @ApiProperty({
    default: 'birthday',
    type: String,
  })
  @IsString()
  birthday: string;
}

export class UpdateUserAvatarDTO {
  buffer: Buffer;
  @IsString()
  filename: string;
  mimeType: string;
}

export class ChangePasswordDTO {
  @ApiProperty({
    default: 'password',
    type: String,
  })
  @IsStrongPassword()
  password: string;
}

export class AdminCreateUserDTO {
  @ApiProperty({
    default: 'username',
    type: String,
  })
  @IsEmail()
  username: string;

  @ApiProperty({
    default: 'password',
    type: String,
  })
  @IsStrongPassword()
  password: string;

  @ApiProperty({
    default: 'TENANT',
    type: String,
  })
  @IsString()
  role: RoleList;
}

export class AdminChangeUserRoleDTO {
  @ApiProperty({
    default: 'TENANT',
    type: String,
  })
  @IsString()
  role: RoleList;
}

export class AdminBanUserDTO {
  @ApiProperty({
    default: true,
    type: Boolean,
  })
  @IsBoolean()
  ban: boolean;
}

export class AdminChangeUserPasswordDTO {
  @ApiProperty({
    default: 'password',
    type: String,
  })
  @IsStrongPassword()
  password: string;
}

export class AdminForceLogoutUserDTO {
  @ApiProperty({
    default: '[]',
    type: Array<String>,
  })
  @ValidateNested()
  @Type(() => String)
  deviceKeys: Array<string>;
}

export class FilterDTO {
  @ApiProperty({
    default: 10,
    type: Number,
  })
  @parseInt()
  @defaultValue(10)
  take: number;

  @ApiProperty({
    default: 0,
    type: Number,
  })
  @parseInt()
  @defaultValue(0)
  skip: number;
}
