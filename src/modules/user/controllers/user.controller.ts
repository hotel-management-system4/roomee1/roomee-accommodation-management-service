import { Controller, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAccessGuard } from 'guards/jwt.access.guard';

@ApiTags('USER API')
@ApiBearerAuth()
@UseGuards(JwtAccessGuard)
@Controller('/api/user')
export class UserController {
  constructor() {}
}
