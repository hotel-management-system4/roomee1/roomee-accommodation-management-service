import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { FirebaseModule, FirebaseModuleOptions } from '@roomee1/firebase';
import { UserController } from './controllers';
import { UserService, IUserService } from './services';

@Module({
  imports: [
    FirebaseModule.forRootAsync({
      useFactory: (configService: ConfigService) => {
        const firebase = configService.get<FirebaseModuleOptions>('firebase');
        return firebase;
      },
      inject: [ConfigService],
    }),
  ],
  controllers: [UserController],
  providers: [
    {
      provide: IUserService,
      useClass: UserService,
    },
  ],
})
export class UserModule {}
