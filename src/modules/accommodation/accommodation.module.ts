import { Module } from '@nestjs/common';
import {
  AccommodationManagerService,
  AccommodationOwnerService,
  AccommodationTenantService,
  IAccommodationManagerService,
  IAccommodationOwnerService,
  IAccommodationTenantService,
} from './services';
import { AccommodationController } from './controllers';

@Module({
  imports: [],
  controllers: [AccommodationController],
  providers: [
    {
      provide: IAccommodationOwnerService,
      useClass: AccommodationOwnerService,
    },
    {
      provide: IAccommodationManagerService,
      useClass: AccommodationManagerService,
    },
    {
      provide: IAccommodationTenantService,
      useClass: AccommodationTenantService,
    },
  ],
})
export class AccommodationModule {}
