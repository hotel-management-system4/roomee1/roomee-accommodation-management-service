import { Injectable } from '@nestjs/common';
import { PrismaService } from '@roomee1/prisma';
import { RoleCheckType } from '../resources/request';
import { UserData } from 'types';
import { GetOwnedAccomodationsResponse } from '../resources/response';
import { ITenantManagerOwnerSharedService } from './shared';

export const IAccommodationManagerService = 'IAccommodationManagerService';
export interface IAccommodationManagerService
  extends ITenantManagerOwnerSharedService {
  getAccommodations(user: UserData): Promise<GetOwnedAccomodationsResponse[]>;
}

@Injectable()
export class AccommodationManagerService
  implements IAccommodationManagerService
{
  constructor(protected readonly prismaService: PrismaService) {}

  async getAccommodations(
    user: UserData,
  ): Promise<GetOwnedAccomodationsResponse[]> {
    const accommodationManager =
      await this.prismaService.accommodationManager.findMany({
        select: {
          accommodationId: true,
        },
        where: {
          managerId: user.id,
        },
      });
    const acommodationIds = accommodationManager.map(
      (accommodation) => accommodation.accommodationId,
    );
    const accommodations = await this.prismaService.accommodation.findMany({
      include: {
        location: true,
      },
      where: {
        id: {
          in: acommodationIds,
        },
      },
    });
    return accommodations;
  }

  async roleCheck(
    accommodationId: string,
    userId: string,
  ): Promise<RoleCheckType> {
    //check if the accommodation existed? and if the user is the Manager of the accommodation.
    const existedAccomodation =
      await this.prismaService.accommodationManager.findFirst({
        where: {
          accommodationId: accommodationId,
          managerId: userId,
        },
      });

    if (!existedAccomodation) {
      return {
        result: false,
        message: 'Accommodation does not exist!',
      };
    }

    if (existedAccomodation.managerId !== userId) {
      return {
        result: false,
        message: `You are not the Manager of this accommodation.`,
      };
    }
    return {
      result: true,
      message: 'true',
    };
  }
}
