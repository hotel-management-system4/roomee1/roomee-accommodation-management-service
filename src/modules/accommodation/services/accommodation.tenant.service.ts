import { Injectable } from '@nestjs/common';
import { PrismaService } from '@roomee1/prisma';
import { GetAccommodationDetailDto } from '../resources/request';
import { UserData } from 'types';
import {
  GetAccommodationDetailResponse,
  GetOwnedAccomodationsResponse,
} from '../resources/response';
import { ITenantManagerOwnerSharedService } from './shared';

export const IAccommodationTenantService = 'IAccommodationTenantService';
export interface IAccommodationTenantService
  extends ITenantManagerOwnerSharedService {
  getAccommodations(user: UserData): Promise<GetOwnedAccomodationsResponse[]>;
  getAccommodationDetail(
    accommodationId: string,
  ): Promise<GetAccommodationDetailResponse>;
}
@Injectable()
export class AccommodationTenantService implements IAccommodationTenantService {
  constructor(protected readonly prismaService: PrismaService) {}

  async getAccommodationDetail(
    accommodationId: string,
  ): Promise<GetAccommodationDetailResponse> {
    const accommodation = await this.prismaService.accommodation.findUnique({
      include: {
        rooms: {
          select: {
            rentCost: true,
            id: true,
            name: true,
            area: true,
            maxRenters: true,
            imagesUrl: true,
            floor: true,
            _count: {
              select: {
                contractTenants: {
                  where: {
                    contract: {
                      endDate: {
                        lte: new Date().toDateString(),
                      },
                    },
                  },
                },
              },
            },
          },
        },
        location: true,
        accommodationManagers: {
          select: {
            managerId: true,
          },
        },
        owner: {
          select: {
            firstName: true,
            lastName: true,
            username: true,
            phoneNumber: true,
            birthday: true,
            imageUrl: true,
          },
        },
      },
      where: {
        id: accommodationId,
      },
    });

    const accommodationManagerList = accommodation.accommodationManagers.map(
      (accommodationManagers) => accommodationManagers.managerId,
    );

    const { accommodationManagers, ...accommodationData } = accommodation;
    return {
      ...accommodationData,
      accommodationManagers: accommodationManagerList,
    };
  }

  async getAccommodations(
    user: UserData,
  ): Promise<GetOwnedAccomodationsResponse[]> {
    const tenants = await this.prismaService.tenant.findMany({
      select: {
        id: true,
      },
      where: {
        userId: user.id,
      },
    });

    const tenantIds = tenants.map((tenant) => tenant.id);
    const contractTenants = await this.prismaService.contractTenant.findMany({
      select: {
        roomId: true,
      },
      where: {
        tenantId: {
          in: tenantIds,
        },
      },
    });

    const roomIds = contractTenants.map(
      (contractTenant) => contractTenant.roomId,
    );
    const rooms = await this.prismaService.room.findMany({
      select: {
        accommodationId: true,
        accommodation: {
          include: {
            location: true,
          },
        },
      },
      where: {
        id: {
          in: roomIds,
        },
      },
    });

    const accommodations = rooms.map((room) => room.accommodation);
    return accommodations;
  }
}
