import { GetOwnedAccomodationsResponse } from 'modules/accommodation/resources/response';
import { UserData } from 'types';

export interface ITenantManagerOwnerSharedService {
  getAccommodations(user: UserData): Promise<GetOwnedAccomodationsResponse[]>;
}
