import { BadRequestException, Injectable } from '@nestjs/common';
import { PrismaService } from '@roomee1/prisma';
import {
  CreateAccommodationDto,
  RoleCheckType,
  UpdateAccommodationDto,
} from '../resources/request';
import { UserData } from 'types';
import {
  CreateAccommodationResponse,
  GetOwnedAccomodationsResponse,
  UpdateAccommodationResponse,
  UserResponse,
} from '../resources/response';
import { AccommodationManagerService } from './accommodation.manager.service';
import { selectUserFragment } from 'types/fragment';
import { ITenantManagerOwnerSharedService } from './shared';

export const IAccommodationOwnerService = 'IAccommodationService';
export interface IAccommodationOwnerService
  extends ITenantManagerOwnerSharedService {
  createAccommodation(
    createAccomodationDto: CreateAccommodationDto,
    user: UserData,
  ): Promise<CreateAccommodationResponse>;
  getAccommodations(user: UserData): Promise<GetOwnedAccomodationsResponse[]>;
  updateAccommodation(
    accommodationId: string,
    updateAccommodationDto: UpdateAccommodationDto,
    user: UserData,
  ): Promise<UpdateAccommodationResponse>;
  deleteAccommodation(accomodationId: string): Promise<void>;
  getManagers(accommodationId: string): Promise<UserResponse[]>;
  getManager(accommodationId: string, managerId: string): Promise<UserResponse>;
}

@Injectable()
export class AccommodationOwnerService
  extends AccommodationManagerService
  implements IAccommodationOwnerService
{
  constructor(protected readonly prismaService: PrismaService) {
    super(prismaService);
  }

  async getManagers(accommodationId: string): Promise<UserResponse[]> {
    const users = await this.prismaService.accommodationManager.findMany({
      where: {
        accommodationId,
      },
      select: {
        user: {
          select: selectUserFragment,
        },
      },
    });

    const resp = users.map(({ user }) => ({
      ...user,
      role: user.role.name,
    }));
    return resp;
  }

  async getManager(
    accommodationId: string,
    managerId: string,
  ): Promise<UserResponse> {
    const manager = await this.prismaService.accommodationManager.findUnique({
      where: {
        compoundId: {
          accommodationId,
          managerId,
        },
      },
      select: {
        user: {
          select: selectUserFragment,
        },
      },
    });

    if (!manager) {
      throw new BadRequestException('not found manager', {
        cause: '',
      });
    }

    const { user } = manager;
    return { ...user, role: user.role.name };
  }

  async deleteAccommodation(accomodationId: string): Promise<void> {
    await this.prismaService.accommodation.update({
      where: {
        id: accomodationId,
      },
      data: {
        deleted: true,
      },
    });
  }

  async createAccommodation(
    createAccomodationDto: CreateAccommodationDto,
    user: UserData,
  ): Promise<CreateAccommodationResponse> {
    const { services, rooms, name, floorNumber, ...accommodationLocation } =
      createAccomodationDto;

    //User cant create a new accommodation if they have already created an accommodation with that name.
    //Multiple users can create accommodations with identical names, but each individual user is restricted from using the same name.
    const isExistAccommodationName = await this.existedAccommodationName(
      name,
      user.id,
    );
    if (isExistAccommodationName) {
      throw new BadRequestException(
        `You have already created an accommodation with the name of ${name}. Please use other names.`,
      );
    }
    // You must initiate a transaction since we need to create the location first.
    // This ensures that if the accommodation creation process fails, the entire transaction will fail.
    // Please see the document on transactions of prisma here:
    // https://www.prisma.io/docs/orm/prisma-client/queries/transactions
    const accomodation = await this.prismaService.$transaction(async (tx) => {
      const location = await tx.location.create({
        data: accommodationLocation,
      });

      const accommodation = await tx.accommodation.create({
        data: {
          name: name,
          ownerId: user.id,
          locationId: location.id,
          floorNumber: floorNumber,
          roomServiceTypes: {
            createMany: {
              data: services,
            },
          },
          rooms: {
            createMany: {
              data: rooms,
            },
          },
        },
      });
      return {
        ...accommodation,
        location,
      };
    });
    return accomodation;
  }

  async getAccommodations(
    user: UserData,
  ): Promise<GetOwnedAccomodationsResponse[]> {
    const accomodations = await this.prismaService.accommodation.findMany({
      include: {
        location: true,
      },
      where: {
        ownerId: user.id,
      },
    });
    return accomodations;
  }

  async updateAccommodation(
    accommodationId: string,
    updateAccommodationDto: UpdateAccommodationDto,
    user: UserData,
  ): Promise<UpdateAccommodationResponse> {
    const { name, floorNumber, ...locationInfo } = updateAccommodationDto;
    //check if the accommodation existed? and if the user is the Owner of the accommodation.
    const existedAccomodation =
      await this.prismaService.accommodation.findUnique({
        where: {
          id: accommodationId,
        },
        select: {
          locationId: true,
        },
      });

    if (!existedAccomodation) {
      throw new BadRequestException('Not found accommodation');
    }

    //check if the owner alreay have an accommodation with the update name
    const existedAccommodationName = await this.existedAccommodationName(
      name,
      user.id,
    );
    if (
      existedAccommodationName &&
      existedAccommodationName.id !== accommodationId
    ) {
      throw new BadRequestException(
        `You have already created an accommodation with the name of ${name}. Please use other names.`,
      );
    }
    // You must initiate a transaction since we need to both update the location and the accommodation info.
    // This ensures that if the accommodation creation process fails, the entire transaction will fail.
    // Please see the document on transactions of prisma here:
    // https://www.prisma.io/docs/orm/prisma-client/queries/transactions
    const accommodation = await this.prismaService.$transaction(async (tx) => {
      const location = await tx.location.update({
        where: {
          id: existedAccomodation.locationId,
        },
        data: locationInfo,
      });

      const accommodation = await tx.accommodation.update({
        where: {
          id: accommodationId,
        },
        data: {
          floorNumber,
          name,
        },
      });
      return {
        ...accommodation,
        location,
      };
    });
    return accommodation;
  }

  override async roleCheck(
    accommodationId: string,
    userId: string,
  ): Promise<RoleCheckType> {
    //check if the accommodation existed? and if the user is the Manager of the accommodation.
    const existedAccomodation =
      await this.prismaService.accommodation.findUnique({
        where: {
          id: accommodationId,
        },
      });

    if (!existedAccomodation) {
      return {
        result: false,
        message: 'Accommodation does not exist!',
      };
    }

    if (existedAccomodation.ownerId !== userId) {
      return {
        result: false,
        message: `You are not the OWNER of this accommodation.`,
      };
    }
    return {
      result: true,
      message: 'true',
    };
  }
  async getCurrentTenants(accommodationId: string) {
    const rooms = await this.prismaService.room.findMany({
      select: {
        contractTenants: {
          where: {
            contract: {
              endDate: {
                lt: Date().toString(),
              },
            },
          },
        },
      },
      where: {
        accommodationId: accommodationId,
      },
    });
  }
  async existedAccommodationName(
    name: string,
    ownerId: string,
  ): Promise<{
    id: string;
  } | null> {
    const exist = await this.prismaService.accommodation.findFirst({
      select: { id: true },
      where: {
        ownerId: ownerId,
        name: name,
      },
    });
    if (exist) {
      return exist;
    }
    return null;
  }
}
