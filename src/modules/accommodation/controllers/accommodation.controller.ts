import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAccessGuard } from 'guards/jwt.access.guard';
import {
  IAccommodationManagerService,
  IAccommodationOwnerService,
  IAccommodationTenantService,
} from '../services';
import {
  CreateAccommodationDto,
  UpdateAccommodationDto,
} from '../resources/request';
import { User } from 'utils/decorator';
import { UserData } from 'types';
import {
  CreateAccommodationResponse,
  GetAccommodationDetailResponse,
  GetOwnedAccomodationsResponse,
} from '../resources/response';
import { UseAuthorized } from 'guards/role.guard';
import { SupportedRole } from 'configurations/role';

@ApiTags('ACCOMMODATION OWNER API')
@ApiBearerAuth()
@UseGuards(JwtAccessGuard)
@Controller('/api/accommodation')
export class AccommodationController {
  private services: Map<
    string,
    | IAccommodationManagerService
    | IAccommodationTenantService
    | IAccommodationOwnerService
  > = null;

  constructor(
    @Inject(IAccommodationManagerService)
    private readonly accommodationManagerService: IAccommodationManagerService,
    @Inject(IAccommodationOwnerService)
    private readonly accommodationOwnerService: IAccommodationOwnerService,
    @Inject(IAccommodationTenantService)
    private readonly accommodationTenantService: IAccommodationTenantService,
  ) {
    this.services = new Map();
    this.services.set(SupportedRole.MANAGER, this.accommodationManagerService);
    this.services.set(SupportedRole.OWNER, this.accommodationOwnerService);
    this.services.set(SupportedRole.TENANT, this.accommodationTenantService);
  }

  @ApiOperation({
    description: `This api is for get all the MANAGER, TENANT and OWNER.\n\n This will return all the accommodations that belongs to own or manage if you are in Manager or Owner role.\n\nAnd will return all the accommodations that you are rentting if you are in Tenant role.`,
  })
  @ApiResponse({
    type: [GetOwnedAccomodationsResponse],
  })
  @Get('/')
  async getAllAccomodations(@User() user: UserData) {
    const result = await this.services.get(user.role).getAccommodations(user);
    return result;
  }

  @ApiOperation({
    description: `This api is for get all the MANAGER, TENANT and OWNER.\n\n This will return all the Ddetail of the accommodation that belongs to own or manage if you are in Manager or Owner role.\n\nAnd will return all the detail of the accommodation that you are rentting if you are in Tenant role.`,
  })
  @ApiResponse({
    type: GetAccommodationDetailResponse,
  })
  @Get('/:id')
  async getAccommodationDetail(@Param('id') id: string) {
    const result =
      await this.accommodationTenantService.getAccommodationDetail(id);
    return result;
  }

  @ApiOperation({
    description:
      'This api is for create a new accommodation. Only account with owner role can create new accommodation.',
  })
  @ApiResponse({
    type: CreateAccommodationResponse,
  })
  @UseAuthorized({ roles: [SupportedRole.OWNER] })
  @Post('/')
  async createAccomodation(
    @Body() createAccomodationDto: CreateAccommodationDto,
    @User() user: UserData,
  ) {
    const result = await this.accommodationOwnerService.createAccommodation(
      createAccomodationDto,
      user,
    );
    return result;
  }

  @ApiOperation({
    description: `This api is for updatting accommodation. Only account with OWNER role can use this api.`,
  })
  @ApiResponse({
    type: UpdateAccommodationDto,
  })
  @UseAuthorized({ roles: [SupportedRole.OWNER] })
  @Patch('/:id')
  async updateAccommodation(
    @Body() updateAccommodationDto: UpdateAccommodationDto,
    @Param('id') accomodationId: string,
    @User() user: UserData,
  ) {
    const result = await this.accommodationOwnerService.updateAccommodation(
      accomodationId,
      updateAccommodationDto,
      user,
    );
    return result;
  }

  @ApiOperation({
    description: `This api is for soft deleting accommodation. Only account with OWNER role can use this api.`,
  })
  @ApiResponse({
    type: String,
    description:
      'Successfully delete the accommodation with id of 828b651d-7a7e-48f7-af08-c19b62dbb0f3',
  })
  @UseAuthorized({ roles: [SupportedRole.OWNER] })
  @Delete('/:id')
  async deleteAccommodation(@Param('id') accomodationId: string) {
    const result =
      await this.accommodationOwnerService.deleteAccommodation(accomodationId);
    return result;
  }

  @ApiOperation({
    description: `This api is for get accommodation's managers. Only account with OWNER role can use this api.`,
  })
  @ApiResponse({
    type: String,
    description: `Successfully get the accommodation's managers with id of 828b651d-7a7e-48f7-af08-c19b62dbb0f3`,
  })
  @UseAuthorized({ roles: [SupportedRole.OWNER] })
  @Get('/:id/manager')
  async getManagers(@Param('id') accomodationId: string) {
    const result =
      await this.accommodationOwnerService.getManagers(accomodationId);
    return result;
  }

  @ApiOperation({
    description: `This api is for get accommodation's manager. Only account with OWNER role can use this api.`,
  })
  @ApiResponse({
    type: String,
    description:
      'Successfully get the accommodation manager with id of 828b651d-7a7e-48f7-af08-c19b62dbb0f3',
  })
  @UseAuthorized({ roles: [SupportedRole.OWNER] })
  @Get('/:id/manager/:managerId')
  async getManager(
    @Param('id') accomodationId: string,
    @Param('managerId') managerId: string,
  ) {
    const result = await this.accommodationOwnerService.getManager(
      accomodationId,
      managerId,
    );
    return result;
  }
}
