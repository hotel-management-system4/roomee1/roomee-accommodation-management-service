import { ApiProperty, OmitType, PartialType } from '@nestjs/swagger';
import { RoleList } from '@prisma/client';
import {
  IsArray,
  IsEmail,
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsString,
  IsUUID,
  ValidateNested,
} from 'class-validator';
import {
  i18nIsEmail,
  i18nIsNotEmpty,
  i18nIsString,
} from 'utils/i18n.validation';

export enum RoomServiceUnit {
  kwh = 'kwh',
  month = 'month',
  m3 = 'm3',
}
export class CreateRoomServiceDto {
  @ApiProperty({
    type: String,
    default: 'electric',
    description: 'The name of the room service.',
  })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    enum: RoomServiceUnit,
    description: 'The unit of cost for the room service.',
  })
  @IsEnum(RoomServiceUnit)
  @IsNotEmpty()
  unit: RoomServiceUnit;

  @ApiProperty({
    type: Number,
    default: 4600000,
    description: 'The cost of the room service.',
  })
  @IsNumber()
  @IsNotEmpty()
  cost: 10000;
}

export class CreateRoomDto {
  @ApiProperty({
    type: Number,
    default: 1,
    description: 'The floor that the room is in.',
  })
  @IsInt()
  @IsNotEmpty()
  floor: number;

  @ApiProperty({
    type: String,
    default: 1,
    description: 'The name of the room.',
  })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    type: Number,
    default: 4600000,
    description: 'The rent cost of the room.',
  })
  @IsNumber()
  @IsNotEmpty()
  rentCost: number;

  @ApiProperty({
    type: Number,
    default: 10,
    description: 'The maximum renters of the room.',
  })
  @IsNumber()
  @IsNotEmpty()
  maxRenters: number;

  @ApiProperty({
    type: Number,
    default: 10,
    description: 'The maximum area of the room.',
  })
  @IsNumber()
  @IsNotEmpty()
  area: number;

  @ApiProperty({
    type: [String],
    default: [
      'https://upload.wikimedia.org/wikipedia/commons/4/4c/Typescript_logo_2020.svg',
      'https://upload.wikimedia.org/wikipedia/commons/4/4c/Typescript_logo_2020.svg',
    ],
    description: 'Images of the room',
  })
  @IsArray()
  imagesUrl?: Array<string> = [];
}

export class CreateAccommodationDto {
  @ApiProperty({
    type: String,
    default: 'myAccomodation',
    description: "The accomodation's name.",
  })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    type: Number,
    default: 3,
    description: "The accomodation's number of floor.",
  })
  @IsInt()
  @IsNotEmpty()
  floorNumber: number;

  @ApiProperty({
    type: String,
    default: 'Viet Nam',
    description: 'The country that the accomodation is located.',
  })
  @IsString()
  @IsNotEmpty()
  country: string;

  @ApiProperty({
    type: String,
    default: 'Ho Chi Minh',
    description: 'The city or province that the accomodation is located.',
  })
  @IsString()
  @IsNotEmpty()
  cityProvince: string;

  @ApiProperty({
    type: String,
    default: 'Thu Duc',
    description: 'The district that the accomodation is located.',
  })
  @IsString()
  @IsNotEmpty()
  district: string;

  @ApiProperty({
    type: String,
    default: 'p.13',
    description: 'The ward that the accomodation is located.',
  })
  @IsString()
  ward?: string;

  @ApiProperty({
    type: String,
    default: 'Linh Trung',
    description: 'The street that the accomodation is located.',
  })
  @IsString()
  @IsNotEmpty()
  street: string;

  @ApiProperty({
    type: [CreateRoomDto],
    description: 'The list of rooms in that accomodation.',
  })
  @IsArray()
  @ValidateNested({ each: true })
  rooms?: Array<CreateRoomDto>;

  @ApiProperty({
    type: [CreateRoomServiceDto],
    description: 'The list of room services in that accomodation.',
  })
  @IsArray()
  @ValidateNested({ each: true })
  services?: Array<CreateRoomServiceDto>;
}

export class GetAccommodationDetailDto {
  @ApiProperty({
    type: String,
    default: '828b651d-7a7e-48f7-af08-c19b62dbb0f3',
  })
  @IsUUID()
  @IsString()
  @IsNotEmpty()
  accommodationId: string;
}
export type RoleCheckType = {
  result: boolean;
  message: string;
};

class ParitalUpdateAccommodationDto
  implements Omit<CreateAccommodationDto, 'rooms' | 'services'>
{
  @ApiProperty({
    type: String,
    default: 'myAccomodation',
    description: "The accomodation's name.",
  })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    type: Number,
    default: 3,
    description: "The accomodation's number of floor.",
  })
  @IsInt()
  @IsNotEmpty()
  floorNumber: number;

  @ApiProperty({
    type: String,
    default: 'Viet Nam',
    description: 'The country that the accomodation is located.',
  })
  @IsString()
  @IsNotEmpty()
  country: string;

  @ApiProperty({
    type: String,
    default: 'Ho Chi Minh',
    description: 'The city or province that the accomodation is located.',
  })
  @IsString()
  @IsNotEmpty()
  cityProvince: string;

  @ApiProperty({
    type: String,
    default: 'Thu Duc',
    description: 'The district that the accomodation is located.',
  })
  @IsString()
  @IsNotEmpty()
  district: string;

  @ApiProperty({
    type: String,
    default: 'p.13',
    description: 'The ward that the accomodation is located.',
  })
  @IsString()
  ward?: string;

  @ApiProperty({
    type: String,
    default: 'Linh Trung',
    description: 'The street that the accomodation is located.',
  })
  @IsString()
  @IsNotEmpty()
  street: string;
}

export class UpdateAccommodationDto extends PartialType(
  ParitalUpdateAccommodationDto,
) {}

export class CreateAccountDto {
  @ApiProperty({
    default: 'username',
    type: String,
  })
  @IsEmail({}, i18nIsEmail)
  username: string;

  @ApiProperty({
    default: 'password',
    type: String,
  })
  @IsString(i18nIsString)
  @IsNotEmpty(i18nIsNotEmpty)
  password: string;

  @ApiProperty({
    default: 'Tran Vinh',
    type: String,
  })
  @IsString(i18nIsString)
  firstName: string;

  @ApiProperty({
    default: 'Phuc',
    type: String,
  })
  @IsString(i18nIsString)
  lastName: string;

  @ApiProperty({
    default: 'MANAGER',
    type: String,
  })
  @IsString(i18nIsString)
  role: RoleList;
}
