import {
  Accommodation,
  AccommodationManager,
  Location,
  Room,
  RoomServiceType,
} from '@prisma/client';
import { RoomServiceUnit } from './request';
import { ApiProperty, OmitType } from '@nestjs/swagger';

export class RoomResponse implements Room {
  @ApiProperty({
    type: String,
    default: 'df3c6635-0bd2-4f4c-b68a-f8555a44e550',
  })
  id: string;

  @ApiProperty({
    type: String,
    default: 'df3c6635-0bd2-4f4c-b68a-f8555a44e550',
  })
  accommodationId: string;

  @ApiProperty({
    type: String,
    default: 'myRoom',
  })
  name: string;

  @ApiProperty({
    type: Number,
    default: 30,
  })
  area: number;

  @ApiProperty({
    type: Number,
    default: 3,
  })
  maxRenters: number;

  @ApiProperty({
    type: [String],
    default: [
      'https://upload.wikimedia.org/wikipedia/commons/4/4c/Typescript_logo_2020.svg',
      'https://upload.wikimedia.org/wikipedia/commons/4/4c/Typescript_logo_2020.svg',
    ],
    description: 'Images of the room',
  })
  imagesUrl: string[];

  @ApiProperty({
    type: Number,
    default: 30,
  })
  rentCost: number;

  @ApiProperty({
    type: Number,
    default: 3,
  })
  floor: number;
}

export class LocationResponse implements Location {
  @ApiProperty({
    type: String,
    default: 'df3c6635-0bd2-4f4c-b68a-f8555a44e550',
  })
  id: string;

  @ApiProperty({
    type: String,
    default: 'Thu Duc',
  })
  street: string;

  @ApiProperty({
    type: String,
    default: 'p.13',
  })
  ward: string;

  @ApiProperty({
    type: String,
    default: 'Linh Trung',
  })
  district: string;

  @ApiProperty({
    type: String,
    default: 'Ho Chi Minh',
  })
  cityProvince: string;

  @ApiProperty({
    type: String,
    default: 'Viet Nam',
  })
  country: string;

  @ApiProperty({
    type: String,
    default: 'my room detail',
  })
  detail: string;
}
export class AccommodationManagerResponse implements AccommodationManager {
  @ApiProperty({
    type: String,
    default: 'df3c6635-0bd2-4f4c-b68a-f8555a44e550',
  })
  accommodationId: string;

  @ApiProperty({
    type: String,
    default: 'df3c6635-0bd2-4f4c-b68a-f8555a44e550',
  })
  managerId: string;
}
export class RoomServiceTypeResponse implements RoomServiceType {
  @ApiProperty({
    type: String,
    default: 'df3c6635-0bd2-4f4c-b68a-f8555a44e550',
  })
  id: string;

  @ApiProperty({
    type: String,
    default: 'df3c6635-0bd2-4f4c-b68a-f8555a44e550',
  })
  accommodationId: string;

  @ApiProperty({
    type: String,
    default: 'parking',
  })
  name: string;
  @ApiProperty({
    enum: RoomServiceUnit,
  })
  unit: string;

  @ApiProperty({
    type: Number,
    default: 300,
  })
  cost: number;
}
export class AccommodationResponse implements Accommodation {
  @ApiProperty({
    type: Boolean,
    default: false,
  })
  deleted: boolean;
  @ApiProperty({
    type: String,
    default: 'df3c6635-0bd2-4f4c-b68a-f8555a44e550',
  })
  id: string;
  @ApiProperty({
    type: String,
    default: '97076cab-89bc-427c-9854-ed1999469940',
  })
  locationId: string;

  @ApiProperty({
    type: String,
    default: '828b651d-7a7e-48f7-af08-c19b62dbb0f3',
  })
  ownerId: string;

  @ApiProperty({
    type: String,
    default: 'myAccomodation',
  })
  name: string;

  @ApiProperty({
    type: Number,
    default: 3,
  })
  floorNumber: number;
}
export class CreateAccommodationResponse extends AccommodationResponse {
  @ApiProperty({
    type: String,
    default: 'df3c6635-0bd2-4f4c-b68a-f8555a44e550',
  })
  id: string;
  @ApiProperty({
    type: String,
    default: '97076cab-89bc-427c-9854-ed1999469940',
  })
  locationId: string;

  @ApiProperty({
    type: String,
    default: '828b651d-7a7e-48f7-af08-c19b62dbb0f3',
  })
  ownerId: string;

  @ApiProperty({
    type: String,
    default: 'myAccomodation',
  })
  name: string;

  @ApiProperty({
    type: Number,
    default: 3,
  })
  floorNumber: number;

  @ApiProperty({
    type: LocationResponse,
  })
  location: LocationResponse;
}

export class GetOwnedAccomodationsResponse extends AccommodationResponse {
  @ApiProperty({
    type: LocationResponse,
  })
  location: LocationResponse;
}
export class count {
  @ApiProperty({
    type: Number,
    default: 200,
  })
  contractTenants: number;
}
export class GetAccommodationRoomResponse extends OmitType(RoomResponse, [
  'accommodationId',
]) {
  @ApiProperty({
    type: count,
  })
  _count: count;
}
export class OwnerInfo {
  @ApiProperty({
    type: String,
    default: 'Andrew',
  })
  firstName: string;
  @ApiProperty({
    type: String,
    default: 'NG',
  })
  lastName: string;

  @ApiProperty({
    type: String,
    default: 'myOwnerUserName',
  })
  username: string;

  @ApiProperty({
    type: String,
    default: '0969486906',
  })
  phoneNumber: string;

  @ApiProperty({
    type: String,
    default: '',
  })
  birthday: string;

  @ApiProperty({
    type: String,
    default: 'avatar',
  })
  imageUrl: string;
}
export class GetAccommodationDetailResponse extends AccommodationResponse {
  @ApiProperty({
    type: [GetAccommodationRoomResponse],
  })
  rooms: Array<GetAccommodationRoomResponse>;

  @ApiProperty({
    type: LocationResponse,
  })
  location: LocationResponse;

  @ApiProperty({
    type: [String],
    default: ['df3c6635-0bd2-4f4c-b68a-f8555a44e550'],
  })
  accommodationManagers: Array<string>;
  owner: OwnerInfo;
}

export class UpdateAccommodationResponse extends AccommodationResponse {
  @ApiProperty({
    type: LocationResponse,
  })
  location: LocationResponse;
}

export class UserResponse {
  @ApiProperty({
    type: String,
    default: 'MyUsername',
  })
  username: string;

  @ApiProperty({
    type: String,
    default: 'Andrew',
  })
  firstName: string;

  @ApiProperty({
    type: String,
    default: 'NG',
  })
  lastName: string;

  @ApiProperty({
    type: String,
    default: '223232a',
  })
  id: string;

  @ApiProperty({
    type: Boolean,
    default: false,
  })
  emailVerified: boolean;

  @ApiProperty({
    type: Boolean,
    default: false,
  })
  phoneVerified: boolean;

  @ApiProperty({
    type: String,
    default: '0969486904',
  })
  phoneNumber: string;

  @ApiProperty({
    type: String,
    default: 'http://google.com/123.png',
  })
  imageUrl: string;

  @ApiProperty({
    type: String,
    default: 'TENANT',
  })
  role: string;

  @ApiProperty({
    type: Boolean,
    default: false,
  })
  locked: boolean;
}
