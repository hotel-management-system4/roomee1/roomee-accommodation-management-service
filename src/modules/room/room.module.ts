import { Module } from '@nestjs/common';
import { IRoomOwnerService, RoomOwnerService } from './services';
import {
  IRoomRepositoryService,
  RoomRepositoryService,
} from './services/room.repostiory.service';
import {
  IRoomManagerService,
  RoomManagerService,
} from './services/room.manager.service';

import {
  IRoomTenantService,
  RoomTenantService,
} from './services/room.tenant.service';
import { RoomController } from './controllers';

@Module({
  imports: [],
  controllers: [RoomController],
  providers: [
    {
      provide: IRoomOwnerService,
      useClass: RoomOwnerService,
    },
    {
      provide: IRoomRepositoryService,
      useClass: RoomRepositoryService,
    },
    {
      provide: IRoomManagerService,
      useClass: RoomManagerService,
    },
    {
      provide: IRoomTenantService,
      useClass: RoomTenantService,
    },
  ],
})
export class RoomModule {}
