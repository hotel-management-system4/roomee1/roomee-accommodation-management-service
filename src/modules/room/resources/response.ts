import { RoomResponse } from 'modules/accommodation/resources/response';

export class GetRoomDetailResponse extends RoomResponse {}
