import { ApiProperty, OmitType, PartialType } from '@nestjs/swagger';

export class GetRoomsByAccommodationDto {
  @ApiProperty({
    type: String,
    default: 'df3c6635-0bd2-4f4c-b68a-f8555a44e550',
  })
  accommodationId: string;
}

export class CreaNewRoomDto {
  @ApiProperty({
    type: String,
    default: 'df3c6635-0bd2-4f4c-b68a-f8555a44e550',
  })
  accommodationId: string;

  @ApiProperty({
    type: String,
    default: 'P.102',
  })
  name: string;

  @ApiProperty({
    type: Number,
    default: 20,
  })
  area: number;

  @ApiProperty({
    type: Number,
    default: 20,
  })
  maxRenters: number;

  @ApiProperty({
    type: [String],
    default: [
      'https://upload.wikimedia.org/wikipedia/commons/4/4c/Typescript_logo_2020.svg',
      'https://upload.wikimedia.org/wikipedia/commons/4/4c/Typescript_logo_2020.svg',
    ],
    description: 'Images of the room',
  })
  imagesUrl: string[];

  @ApiProperty({
    type: Number,
    default: 20,
  })
  rentCost: number;

  @ApiProperty({
    type: Number,
    default: 5,
  })
  floor: number;
}

export class UpdateRoomDto extends PartialType(
  OmitType(CreaNewRoomDto, ['accommodationId']),
) {}
