import { BadRequestException, Injectable } from '@nestjs/common';
import { PrismaService } from '@roomee1/prisma';
import { GetRoomDetailResponse } from '../resources/response';
import { CreaNewRoomDto, UpdateRoomDto } from '../resources/request';

export const IRoomRepositoryService = 'IRoomRepositoryService';

export interface IRoomRepositoryService {
  findMany(accommodationId: string): Promise<GetRoomDetailResponse[]>;
  findOne(roomId: string): Promise<GetRoomDetailResponse>;
  update(
    roomId: string,
    updateRoomDto: UpdateRoomDto,
  ): Promise<GetRoomDetailResponse>;
  create(createRoomDto: CreaNewRoomDto): Promise<GetRoomDetailResponse>;
  delete(roomId: string): Promise<GetRoomDetailResponse>;
}
@Injectable()
export class RoomRepositoryService implements IRoomRepositoryService {
  constructor(private readonly prismaService: PrismaService) {}
  async findMany(accommodationId: string): Promise<GetRoomDetailResponse[]> {
    const rooms = await this.prismaService.room.findMany({
      where: {
        accommodationId: accommodationId,
      },
    });
    return rooms;
  }

  async findOne(roomId: string): Promise<GetRoomDetailResponse> {
    const room = await this.prismaService.room.findUnique({
      where: {
        id: roomId,
      },
    });
    if (!room) {
      throw new BadRequestException('Room not found!');
    }
    return room;
  }

  async create(createRoomDto: CreaNewRoomDto): Promise<GetRoomDetailResponse> {
    const room = await this.prismaService.room.create({
      data: {
        ...createRoomDto,
      },
    });
    if (!room) {
      throw new BadRequestException(
        `Something wrong. Can not create the room.`,
      );
    }
    return room;
  }

  async update(
    roomId: string,
    updateRoomDto: UpdateRoomDto,
  ): Promise<GetRoomDetailResponse> {
    const room = await this.prismaService.room.update({
      where: {
        id: roomId,
      },
      data: {
        ...updateRoomDto,
      },
    });

    if (!room) {
      throw new BadRequestException(
        `Something wrong. Can not update the room information.`,
      );
    }
    return room;
  }

  async delete(roomId: string): Promise<GetRoomDetailResponse> {
    const room = await this.prismaService.room.delete({
      where: {
        id: roomId,
      },
    });
    return room;
  }
}
