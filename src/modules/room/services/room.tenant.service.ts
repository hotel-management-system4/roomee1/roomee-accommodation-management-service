import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { PrismaService } from '@roomee1/prisma';
import { UserData } from 'types';
import { GetRoomDetailResponse } from '../resources/response';
import { IRoomRepositoryService } from './room.repostiory.service';

export const IRoomTenantService = 'IRoomTenantService';

export interface IRoomTenantService {
  getAllRooms(accommodationId: string): Promise<GetRoomDetailResponse[]>;
  getRoomDetail(roomId: string): Promise<GetRoomDetailResponse>;
}

@Injectable()
export class RoomTenantService implements IRoomTenantService {
  constructor(
    protected readonly prismaService: PrismaService,
    @Inject(IRoomRepositoryService)
    protected readonly roomRepositoryService: IRoomRepositoryService,
  ) {}

  async getAllRooms(accommodationId: string): Promise<GetRoomDetailResponse[]> {
    const rooms = await this.roomRepositoryService.findMany(accommodationId);
    return rooms;
  }

  async getRoomDetail(roomId: string): Promise<GetRoomDetailResponse> {
    const room = await this.roomRepositoryService.findOne(roomId);
    return room;
  }

  //TODO: move to policies
  async isTenantOfThisRoom(roomId: string, user: UserData): Promise<Boolean> {
    const contractTenant = await this.prismaService.contractTenant.findFirst({
      where: {
        id: roomId,
      },
    });
    if (!contractTenant) {
      return false;
    }
    if (contractTenant.tenantId === user.id) {
      return true;
    }

    return false;
  }
  //TODO: move to policies
  async isTenantOfThisAccommodation(
    accommodationId: string,
    user: UserData,
  ): Promise<Boolean> {
    const tenant = await this.prismaService.$queryRaw`
    select contract_tenant.id from accommodation
    left join room on room.accommodation_id=accommodation.id
    left join contract_tenant on contract_tenant.room_id= room.id where 
    contract_tenant.id=${user.id} and accommodation.id=${accommodationId}
    `;

    return false;
  }
}
