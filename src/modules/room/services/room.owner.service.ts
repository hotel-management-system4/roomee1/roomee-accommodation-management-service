import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { PrismaService } from '@roomee1/prisma';
import { UserData } from 'types';
import { GetRoomDetailResponse } from '../resources/response';
import { IRoomRepositoryService } from './room.repostiory.service';
import { CreaNewRoomDto, UpdateRoomDto } from '../resources/request';

export const IRoomOwnerService = 'IRoomOwnerService';

export interface IRoomOwnerService {
  createNewRoom(createRoomDto: CreaNewRoomDto): Promise<GetRoomDetailResponse>;
  updateRoom(
    roomId: string,
    updateRoomDto: UpdateRoomDto,
  ): Promise<GetRoomDetailResponse>;
  deleteRoom(roomId: string): Promise<GetRoomDetailResponse>;
}

@Injectable()
export class RoomOwnerService implements IRoomOwnerService {
  constructor(
    protected readonly prismaService: PrismaService,
    @Inject(IRoomRepositoryService)
    protected readonly roomRepositoryService: IRoomRepositoryService,
  ) {}
  async updateRoom(
    roomId: string,
    updateRoomDto: UpdateRoomDto,
  ): Promise<GetRoomDetailResponse> {
    const room = await this.roomRepositoryService.update(roomId, updateRoomDto);
    return room;
  }

  async deleteRoom(roomId: string): Promise<GetRoomDetailResponse> {
    const room = await this.roomRepositoryService.delete(roomId);
    return room;
  }

  async createNewRoom(
    createRoomDto: CreaNewRoomDto,
  ): Promise<GetRoomDetailResponse> {
    const room = await this.roomRepositoryService.create(createRoomDto);
    return room;
  }

  // move to policies
  async isAllowedToEditRoom(roomId: string, user: UserData): Promise<Boolean> {
    const room = await this.prismaService.room.findFirst({
      include: {
        accommodation: true,
      },
      where: {
        id: roomId,
      },
    });
    if (!room) {
      return false;
    }
    if (room.accommodation.ownerId === user.id) {
      return true;
    }

    return false;
  }

  // move to policies
  async isAllowedtoEditAccommodation(
    accommodationId: string,
    user: UserData,
  ): Promise<Boolean> {
    const accommodation = await this.prismaService.accommodation.findUnique({
      where: {
        id: accommodationId,
      },
    });
    if (!accommodation) {
      return false;
    }
    if (accommodation.ownerId === user.id) {
      return true;
    }
    return false;
  }
}
