import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { PrismaService } from '@roomee1/prisma';
import { UserData } from 'types';
import { GetRoomDetailResponse } from '../resources/response';
import { IRoomRepositoryService } from './room.repostiory.service';
import { CreaNewRoomDto, UpdateRoomDto } from '../resources/request';
import { RoomOwnerService } from './room.owner.service';

export const IRoomManagerService = 'IRoomManagerService';

export interface IRoomManagerService {
  createNewRoom(createRoomDto: CreaNewRoomDto): Promise<GetRoomDetailResponse>;
  updateRoom(
    roomId: string,
    updateRoomDto: UpdateRoomDto,
  ): Promise<GetRoomDetailResponse>;
  deleteRoom(roomId: string): Promise<GetRoomDetailResponse>;
}

@Injectable()
export class RoomManagerService
  extends RoomOwnerService
  implements IRoomManagerService
{
  constructor(
    protected readonly prismaService: PrismaService,
    @Inject(IRoomRepositoryService)
    protected readonly roomRepositoryService: IRoomRepositoryService,
  ) {
    super(prismaService, roomRepositoryService);
  }

  // move to policies
  override async isAllowedToEditRoom(
    roomId: string,
    user: UserData,
  ): Promise<Boolean> {
    const room = await this.prismaService.room.findFirst({
      include: {
        accommodation: {
          include: {
            accommodationManagers: true,
          },
        },
      },
      where: {
        id: roomId,
      },
    });
    if (!room) {
      return false;
    }
    //array.sum, bluebird
    for (const accommodationManagers of room.accommodation
      .accommodationManagers) {
      if (accommodationManagers.managerId === user.id) {
        return true;
      }
    }
    return false;
  }

  // move to policies
  async isAllowedtoEditAccommodation(
    accommodationId: string,
    user: UserData,
  ): Promise<Boolean> {
    const accommodation = await this.prismaService.accommodation.findUnique({
      include: {
        accommodationManagers: true,
      },
      where: {
        id: accommodationId,
      },
    });
    if (!accommodation) {
      return false;
    }
    for (const accommodationManagers of accommodation.accommodationManagers) {
      if (accommodationManagers.managerId === user.id) {
        return true;
      }
    }
    return false;
  }
}
