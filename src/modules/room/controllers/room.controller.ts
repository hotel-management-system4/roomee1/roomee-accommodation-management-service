import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAccessGuard } from 'guards/jwt.access.guard';
import { UserData } from 'types';
import { User } from 'utils/decorator';

import {
  CreaNewRoomDto,
  GetRoomsByAccommodationDto,
  UpdateRoomDto,
} from '../resources/request';
import {
  IRoomTenantService,
  IRoomManagerService,
  IRoomOwnerService,
} from '../services';
import { SupportedRole } from 'configurations/role';
import { UseAuthorized } from 'guards/role.guard';
import { GetRoomDetailResponse } from '../resources/response';

@ApiTags('ROOM API')
@ApiBearerAuth()
@UseGuards(JwtAccessGuard)
@Controller('/api/room/')
export class RoomController {
  private services: Map<string, IRoomOwnerService | IRoomManagerService> = null;

  constructor(
    @Inject(IRoomTenantService)
    private readonly roomTenantService: IRoomTenantService,
    @Inject(IRoomManagerService)
    readonly roomManagerService: IRoomManagerService,
    @Inject(IRoomOwnerService) readonly roomOwnerService: IRoomOwnerService,
  ) {
    this.services = new Map();
    this.services.set(SupportedRole.MANAGER, this.roomManagerService);
    this.services.set(SupportedRole.OWNER, this.roomOwnerService);
  }

  @ApiOperation({
    description: `This api is for all the MANAGER, TENANT and OWNER.\n\n This will return all the rooms that belongs to own or manage if you are in Manager or Owner role.\n\nAnd will return all the rooms that you are rentting if you are in Tenant role.`,
  })
  @ApiResponse({
    type: [GetRoomDetailResponse],
  })
  @Get('/')
  async getAllRooms(@Query() { accommodationId }: GetRoomsByAccommodationDto) {
    const result = await this.roomTenantService.getAllRooms(accommodationId);
    return result;
  }

  @ApiOperation({
    description: `This api is for all the MANAGER, TENANT and OWNER.\n\n This will return all the rooms that belongs to own or manage if you are in Manager or Owner role.\n\nAnd will return  the rooms detail that you are rentting if you are in Tenant role.`,
  })
  @ApiResponse({
    type: GetRoomDetailResponse,
  })
  @Get('/:roomId')
  async getRoomDetail(@Param('roomId') roomId: string, @User() user: UserData) {
    const result = await this.roomTenantService.getRoomDetail(roomId);
    return result;
  }

  @ApiOperation({
    description: `This api is for the MANAGER,and OWNER.\n\n This will let the owner and manager create the room.`,
  })
  @ApiResponse({
    type: GetRoomDetailResponse,
  })
  @UseAuthorized({ roles: [SupportedRole.MANAGER, SupportedRole.OWNER] })
  @Post('/')
  async createRoom(
    @Body() createRoomDto: CreaNewRoomDto,
    @User() user: UserData,
  ) {
    const result = await this.services
      .get(user.role)
      .createNewRoom(createRoomDto);
    return result;
  }

  @ApiOperation({
    description: `This api is for the MANAGER,and OWNER.\n\n This will let the owner and manager update the room.`,
  })
  @ApiResponse({
    type: GetRoomDetailResponse,
  })
  @UseAuthorized({ roles: [SupportedRole.MANAGER, SupportedRole.OWNER] })
  @Patch('/:roomId')
  async updateRoom(
    @Param('roomId') roomId: string,
    @Body() updateRoomDto: UpdateRoomDto,
    @User() user: UserData,
  ) {
    const result = await this.services
      .get(user.role)
      .updateRoom(roomId, updateRoomDto);
    return result;
  }

  @ApiOperation({
    description: `This api is for the MANAGER,and OWNER.\n\n This will let the owner and manager delete the room.`,
  })
  @ApiResponse({
    type: GetRoomDetailResponse,
  })
  @UseAuthorized({ roles: [SupportedRole.MANAGER, SupportedRole.OWNER] })
  @Delete('/:roomId')
  async deleteRoom(@Param('roomId') roomId: string, @User() user: UserData) {
    const result = await this.services.get(user.role).deleteRoom(roomId);
    return result;
  }
}
