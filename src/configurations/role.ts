export const ROLES_KEY = 'roles';
export const POLICIES_KEY = 'polices';

export enum SupportedRole {
  ADMIN = 'ADMIN',
  TENANT = 'TENANT',
  MOD = 'MOD',
  OWNER = 'OWNER',
  MANAGER = 'MANAGER',
}
