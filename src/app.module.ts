import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { LoggerModule, RedisCacheModule, ScryptModule } from '@roomee1/common';
import { PrismaModule } from '@roomee1/prisma';
import { SendgridModule, SendgridModuleOptions } from '@roomee1/sendgrid';
import {
  accessToken,
  cache,
  facebook,
  firebase,
  google,
  refreshToken,
  sendgrid,
} from 'configurations/config';
import { AccommodationModule } from 'modules/accommodation/accommodation.module';
import { RoomModule } from 'modules/room/room.module';
import { UserModule } from 'modules/user/user.module';
import { CacheOptions } from 'types';

@Module({
  imports: [
    UserModule,
    AccommodationModule,
    RoomModule,
    LoggerModule.register({
      service: 'roomee-auth-service',
      isGlobal: true,
    }),
    JwtModule.register({
      global: true,
    }),
    ConfigModule.forRoot({
      load: [
        facebook,
        google,
        accessToken,
        refreshToken,
        cache,
        sendgrid,
        firebase,
      ],
      isGlobal: true,
    }),
    // RedisCacheModule.registerAsync({
    //   isGlobal: true,
    //   useFactory: (configService: ConfigService) => {
    //     const options = configService.get<CacheOptions>('cache');
    //     return {
    //       host: options.url,
    //       name: 'access',
    //     };
    //   },
    //   inject: [ConfigService],
    // }),
    ScryptModule.register({ isGlobal: true }),
    SendgridModule.forRootAsync({
      global: true,
      useFactory: (configService: ConfigService) => {
        const options = configService.get<SendgridModuleOptions>('sendgrid');
        return options;
      },
      inject: [ConfigService],
    }),
    PrismaModule.forRoot({ isGlobal: true }),
  ],
})
export class AppModule {}
