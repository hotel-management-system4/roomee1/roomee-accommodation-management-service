import { APIGatewayEvent, Callback, Context, Handler } from 'aws-lambda';
import { bootstrap } from '../../bootstrap';

export default async (
  event: APIGatewayEvent,
  context: Context,
  callback: Callback,
) => {
  // only if "Lambda proxy integration" is enabled in API method.
  if (event.pathParameters) {
    event.pathParameters.proxy = event.path.slice(1);
  }

  const server = await bootstrap();
  return server(event, context, callback);
};

export const getApiDocs = async (
  event: APIGatewayEvent,
  context: Context,
  callback: Callback,
) => {
  const server = await bootstrap({ enableSwagger: true });
  return server(event, context, callback);
};
