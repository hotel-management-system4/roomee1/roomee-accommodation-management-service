import { DeviceData, TrackingData, UserData } from 'types';

declare module '@types/express-serve-static-core' {
  interface Request {
    user?: UserData;
    tracking?: TrackingData;
    device?: DeviceData;
  }
}
