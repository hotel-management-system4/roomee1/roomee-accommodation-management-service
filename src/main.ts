import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { setupSwagger, validationError } from './bootstrap';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: true,
  });

  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: validationError,
    }),
  );

  setupSwagger(app);
  await app.listen(3001);
}
bootstrap();
